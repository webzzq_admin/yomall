package com.zzq.tenant.mult;


import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

public class DynamicDataSource extends AbstractRoutingDataSource {

    public  DynamicDataSource(DataSource defaultTargetSource, Map<Object,Object> targetDataSources){
        //defaultTargetSource：默认数据源，默认ds1
        //targetDataSources：目标数据源列表：ds1、ds2
        super.setDefaultTargetDataSource(defaultTargetSource);
        super.setTargetDataSources(targetDataSources);
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDataSourceType();
    }

    public void setDataSources(Map<Object, Object> dataSources) {
        super.setTargetDataSources(dataSources);
    }
}

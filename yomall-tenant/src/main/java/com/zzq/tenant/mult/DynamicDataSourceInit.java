package com.zzq.tenant.mult;

import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariDataSource;
import com.zzq.tenant.constant.DataSourceConstant;
import com.zzq.tenant.pojo.Tenant;
import com.zzq.tenant.service.TenantService;
import com.zzq.tenant.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 动态数据源
 *
 * @author wulongbo
 * @version 1.0
 * @Description 动态数据源初始化
 * @date 2020/12/5 11:11
 */
@Slf4j
@Configuration
public class DynamicDataSourceInit {
    @Autowired
    private TenantService tenantInfoService;

    @Bean
    public void initDataSource() {
        log.info("======初始化动态数据源=====");
        DynamicDataSource dynamicDataSource = (DynamicDataSource) SpringContextUtils.getBean("dynamicDataSource");
        HikariDataSource master = (HikariDataSource) SpringContextUtils.getBean("master");
        Map<Object, Object> dataSourceMap = new HashMap<>();
        dataSourceMap.put("master", master);
        List<Tenant> tenantList = tenantInfoService.selectList();
        for (Tenant tenantInfo : tenantList) {
            DruidDataSource dataSource = new DruidDataSource();
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl("jdbc:mysql://localhost:3306/"+ DataSourceConstant.SOURCECODE+tenantInfo.getCode() +"?serverTimezone=UTC&useUnicode=true@characterEncoding=utf-8\n");
            dataSource.setUsername("root");
            dataSource.setPassword("root");
            dataSource.setConnectProperties(master.getDataSourceProperties());
            dataSourceMap.put(tenantInfo.getId(), dataSource);
            System.out.println("---"+dataSource);
        }
        // 设置数据源
        dynamicDataSource.setDataSources(dataSourceMap);
        /**
         * 必须执行此操作，才会重新初始化AbstractRoutingDataSource 中的 resolvedDataSources，也只有这样，动态切换才会起效
         */
        dynamicDataSource.afterPropertiesSet();
    }
}
package com.zzq.tenant.mult;

public class DynamicDataSourceContextHolder {

    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    /**
     * 设置数据源变量
     * @param dataSourceType
     */
    public static void setDataSourceType(String dataSourceType){
        System.out.printf("切换到了{%s}数据源\n",dataSourceType);
        CONTEXT_HOLDER.set(dataSourceType);
    }

    /**
     * 获取数据源变量
     * @return
     */
    public static String  getDataSourceType(){
        return CONTEXT_HOLDER.get();
    }

    /***
     * 清空数据源变量
     */
    public static void clearDateSourceType(){
        CONTEXT_HOLDER.remove();
    }

}

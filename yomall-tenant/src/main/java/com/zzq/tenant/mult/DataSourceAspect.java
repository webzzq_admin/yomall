package com.zzq.tenant.mult;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Order(1)
@Component
public class DataSourceAspect {

    @Pointcut("@annotation(com.zzq.tenant.mult.DataSourceAnnotation)")
    public void dsPointCut() {

    }

    @Around("dsPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        DataSourceAnnotation dataSourceAnnotation = method.getAnnotation(DataSourceAnnotation.class);
        System.out.println("方法执行前，切入逻辑...");
        if (dataSourceAnnotation != null) {
            DynamicDataSourceContextHolder.setDataSourceType(dataSourceAnnotation.value().name());
        }
        try {
            //继续执行方法
            Object result = point.proceed();
            System.out.println("方法执行后，切入逻辑...");
            return result;
        } finally {
            //释放ThreadLocal变量
            DynamicDataSourceContextHolder.clearDateSourceType();
        }
    }

}

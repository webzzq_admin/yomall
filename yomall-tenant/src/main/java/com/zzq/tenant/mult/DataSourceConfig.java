package com.zzq.tenant.mult;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DataSourceConfig {

    @Bean
    @ConfigurationProperties("spring.datasource.ds1")
    public DataSource ds1() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.ds2")
    public DataSource ds2() {
        return DataSourceBuilder.create().build();
    }

    @Bean("master")
    @Primary
    @ConfigurationProperties("spring.datasource.ds3")
    public DataSource master() {
        return DataSourceBuilder.create().build();
    }


    @Bean("dynamicDataSource")
    public DynamicDataSource dataSource(DataSource ds1, DataSource ds2) {
        DataSource defaultTargetSource = master();
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DataSourceType.DS1.name(), ds1);
        targetDataSources.put(DataSourceType.DS2.name(), ds2);
        return new DynamicDataSource(defaultTargetSource, targetDataSources);
    }

}

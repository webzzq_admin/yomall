package com.zzq.tenant.enums;

/**
 * @author mszhou
 * @title: DataSourceType
 * @projectName yomall-third-party
 * @description: TODO
 * @date 2021/2/275:08 下午
 */
public enum DataSourceType {
    Ds1,
    Ds2,
    Ds3
}

package com.zzq.tenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author mszhou
 * @title: App
 * @projectName yomall-third-party
 * @description: TODO
 * @date 2021/2/266:12 下午
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class YomallTenantApplication {
    public static void main(String[] args) {
        SpringApplication.run(YomallTenantApplication.class);
    }
}

package com.zzq.tenant.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author mszhou
 * @title: Tenant
 * @projectName yomall-third-party
 * @description: TODO
 * @date 2021/3/13:30 下午
 */
@Data
@TableName(value = "c_tenant")
public class Tenant  {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    private String code;

    private String name;

    private String type;

    private String connectType;

    private String status;

    private Boolean readonly_;

    private String duty;

    private Date expirationTime;

    private String logo;

    private String describe_;

    private Date createTime;

    private Long createdBy;

    private Date updateTime;

    private Long updatedBy;


}


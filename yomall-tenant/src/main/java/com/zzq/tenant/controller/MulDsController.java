package com.zzq.tenant.controller;

import com.zzq.tenant.mult.DataSourceAnnotation;
import com.zzq.tenant.mult.DataSourceType;
import com.zzq.tenant.mult.DynamicDataSource;
import com.zzq.tenant.mult.DynamicDataSourceContextHolder;
import com.zzq.tenant.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author mszhou
 * @title: MulDsController
 * @projectName yomall-third-party
 * @description: 多数据源测试
 * @date 2021/2/275:13 下午
 */
@RestController
public class MulDsController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private TenantService tenantService;

    @GetMapping("/ds1")
    @DataSourceAnnotation(value = com.zzq.tenant.mult.DataSourceType.DS1)
    public List<Map<String,Object>> ds1(){
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select  * from user");
        return maps;
    }


    @GetMapping("/ds2")
    public List<Map<String,Object>> ds2(String code){
        DynamicDataSourceContextHolder.setDataSourceType(code);
        System.out.println(DynamicDataSourceContextHolder.getDataSourceType());
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from c_user");
        return maps;
    }

    @GetMapping("/ds3")
    public List ds3(){
        List list = tenantService.selectList();
        System.out.println(DynamicDataSourceContextHolder.getDataSourceType());
        return list;
    }




}

package com.zzq.tenant.constant;

/**
 * @author mszhou
 * @title: DataSourceConstant
 * @projectName yomall-third-party
 * @description: 数据源前缀
 * @date 2021/3/13:19 下午
 */
public class DataSourceConstant {

    public static final String SOURCECODE = "yomall_base_";

}

package com.zzq.tenant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzq.tenant.pojo.Tenant;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author mszhou
 * @title: TenantMapper
 * @projectName yomall-third-party
 * @description: 租户默认数据源
 * @date 2021/3/13:53 下午
 */
@Mapper
public interface TenantMapper extends BaseMapper<Tenant> {
}

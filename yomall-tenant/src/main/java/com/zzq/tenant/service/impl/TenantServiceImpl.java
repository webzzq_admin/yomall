package com.zzq.tenant.service.impl;

import com.zzq.tenant.mapper.TenantMapper;
import com.zzq.tenant.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author mszhou
 * @title: TenantServiceImpl
 * @projectName yomall-third-party
 * @description: TODO
 * @date 2021/3/17:57 下午
 */
@Service
public class TenantServiceImpl implements TenantService {

    @Autowired
    private TenantMapper tenantMapper;

    @Override
    public void login(String code) {

    }

    @Override
    public List selectList() {
        return tenantMapper.selectList(null);
    }
}

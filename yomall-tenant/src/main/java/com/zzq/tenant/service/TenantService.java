package com.zzq.tenant.service;

import java.util.List;
import java.util.Map;

/**
 * @author mszhou
 * @title: TenantService
 * @projectName yomall-third-party
 * @description: TODO
 * @date 2021/3/13:17 下午
 */
public interface TenantService {
    /**
     * 租户登录
     * @param code
     */
    void login(String code);

    /**
     * 查询所有租户数据源
     * @return
     */
    List selectList();
}

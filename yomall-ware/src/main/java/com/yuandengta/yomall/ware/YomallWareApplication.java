package com.yuandengta.yomall.ware;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author mszhou
 */
@SpringBootApplication
@EnableDiscoveryClient
public class YomallWareApplication {
    public static void main(String[] args) {
        SpringApplication.run(YomallWareApplication.class);
    }
}

package com.yuandengta.yomall.ware.dao;

import com.yuandengta.yomall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:32:19
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}

package com.yuandengta.yomall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuandengta.common.utils.PageUtils;
import com.yuandengta.yomall.ware.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:32:19
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}


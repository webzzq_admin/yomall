package com.yuandengta.yomall.ware.dao;

import com.yuandengta.yomall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:32:19
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}

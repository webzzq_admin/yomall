package com.yuandengta.yomall.yomallgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author mszhou
 */
@EnableDiscoveryClient
@SpringBootApplication
public class YomallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(YomallGatewayApplication.class, args);
    }

}

package com.yuandengta.yomall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuandengta.common.utils.PageUtils;
import com.yuandengta.yomall.order.entity.OrderSettingEntity;

import java.util.Map;

/**
 * 订单配置信息
 *
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:27:41
 */
public interface OrderSettingService extends IService<OrderSettingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}


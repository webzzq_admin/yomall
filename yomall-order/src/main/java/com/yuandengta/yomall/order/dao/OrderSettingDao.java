package com.yuandengta.yomall.order.dao;

import com.yuandengta.yomall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:27:41
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {
	
}

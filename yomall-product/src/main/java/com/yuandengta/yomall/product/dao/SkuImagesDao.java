package com.yuandengta.yomall.product.dao;

import com.yuandengta.yomall.product.entity.SkuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku图片
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:29:13
 */
@Mapper
public interface SkuImagesDao extends BaseMapper<SkuImagesEntity> {
	
}

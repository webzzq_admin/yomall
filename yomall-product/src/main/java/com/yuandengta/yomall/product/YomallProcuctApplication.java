package com.yuandengta.yomall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author mszhou
 */
@MapperScan("com.yuandengta.yomall.product.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class YomallProcuctApplication {
    public static void main(String[] args) {
        SpringApplication.run(YomallProcuctApplication.class);
    }
}

package com.yuandengta.yomall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuandengta.common.utils.PageUtils;
import com.yuandengta.yomall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:29:13
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询所有分类及其子分类，树形结构
     * @return
     */
    List<CategoryEntity> listWithTree();
}


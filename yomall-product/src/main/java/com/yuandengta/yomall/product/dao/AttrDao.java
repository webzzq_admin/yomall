package com.yuandengta.yomall.product.dao;

import com.yuandengta.yomall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:29:13
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}

package com.yuandengta.yomall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yuandengta.yomall.product.entity.BrandEntity;
import com.yuandengta.yomall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 基本的单元测试
 */
@SpringBootTest
class YomallProductApplicationTests {

    @Autowired
    private BrandService brandService;

    /**
     * 测试插入
     */
    @Test
    void testSave() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("华为");
        brandService.save(brandEntity);
    }

    /**
     * 测试更新
     */
    @Test
    void testUpdate() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setBrandId(3L);
        brandEntity.setDescript("华为-民族品牌的骄傲");
        brandService.updateById(brandEntity);
    }

    /**
     * 测试查询
     */
    @Test
    void testQuery() {
        // 支持分页查询、批量
        List<BrandEntity> list = brandService.list(
                new QueryWrapper<BrandEntity>()
                        .eq("brand_id", 3L));

        list.forEach(item -> {
            System.out.println(item);
        });
    }

}

package com.yuandengta.yomall.member.dao;

import com.yuandengta.yomall.member.entity.IntegrationChangeHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:31:19
 */
@Mapper
public interface IntegrationChangeHistoryDao extends BaseMapper<IntegrationChangeHistoryEntity> {
	
}

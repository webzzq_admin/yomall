package com.yuandengta.yomall.member;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author mszhou
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.yuandengta.yomall.member.feign")
public class YomallMemberApplication {
    public static void main(String[] args) {
        SpringApplication.run(YomallMemberApplication.class);
    }
}

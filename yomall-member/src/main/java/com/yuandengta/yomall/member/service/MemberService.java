package com.yuandengta.yomall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuandengta.common.utils.PageUtils;
import com.yuandengta.yomall.member.entity.MemberEntity;

import java.util.Map;

/**
 * 
 *
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:31:19
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);
}


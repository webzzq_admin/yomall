package com.yuandengta.yomall.member.feign;


import com.yuandengta.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author mszhou
 */
@FeignClient("yomall-coupon")
public interface CouponFeignService {

    /**
     * 调用yomall-coupon 远程服务的接口
     * @param id
     * @return
     */
    @RequestMapping("/coupon/coupon/info/{id}")
    public R info(@PathVariable("id") Long id);

}

package com.yuandengta.yomall;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author mszhou
 */
@EnableDiscoveryClient
@SpringBootApplication
public class YomallCouponApplcation {
    public static void main(String[] args) {
        SpringApplication.run(YomallCouponApplcation.class);
    }
}

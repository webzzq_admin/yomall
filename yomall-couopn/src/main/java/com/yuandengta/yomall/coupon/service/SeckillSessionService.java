package com.yuandengta.yomall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuandengta.common.utils.PageUtils;
import com.yuandengta.yomall.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 
 *
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:30:18
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}


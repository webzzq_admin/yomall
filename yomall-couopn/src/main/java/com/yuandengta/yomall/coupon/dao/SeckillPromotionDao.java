package com.yuandengta.yomall.coupon.dao;

import com.yuandengta.yomall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:30:18
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}

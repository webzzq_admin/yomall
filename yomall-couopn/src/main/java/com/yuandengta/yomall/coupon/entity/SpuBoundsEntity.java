package com.yuandengta.yomall.coupon.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author mszhou
 * @email 2486827339@qq.com
 * @date 2021-01-21 19:30:18
 */
@Data
@TableName("sms_spu_bounds")
public class SpuBoundsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private Long spuId;
	/**
	 * 
	 */
	private BigDecimal growBounds;
	/**
	 * 
	 */
	private BigDecimal buyBounds;
	/**
	 * 
	 */
	private Integer work;

}
